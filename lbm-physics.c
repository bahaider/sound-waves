/***
    This file is part of "sound-waves" program.
    This program simulates sound waves behaviour in 2D space.
    Copyright (C) 2014  Sergey Dukanov <sergeydukanov@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lbm-physics.h"

void allocate_memory();
void init_speed_rho();
void init_f();
void init_objects();

void streaming (SimulationState *state);
void compute_speed_rho (SimulationState *state);
void compute_f_equilibrium (SimulationState *state);
void collide (SimulationState *state);
void objects (SimulationState *state);

static const float w0 = 16 / 36.0;
static const float w1 = 4 / 36.0;
static const float w2 = 1 / 36.0;

static const float tau = 1.0;
  
static const float w[9] = {
  16 / 36.0, 
  4  / 36.0, 
  4  / 36.0, 
  4  / 36.0, 
  4  / 36.0,
  1  / 36.0,
  1  / 36.0,
  1  / 36.0,
  1  / 36.0
};

static int c[Q][2] = {
  {0, 0},
  {1, 0}, {0, 1}, {-1, 0}, {0, -1},
  {1, 1}, {-1, 1}, {-1, -1}, {1, -1}
};

/* creating an object, which contains a simulation state  */
SimulationState *simulation_create (int nx, int ny) {
  SimulationState *state;
  
  state = (SimulationState *) malloc(sizeof(SimulationState));
  state->nx = nx;
  state->ny = ny;
  allocate_memory (state);
  return state;
}

void simulation_init (SimulationState *state) {
  init_speed_rho (state);
  compute_f_equilibrium (state);
  init_f (state);
  init_objects (state);
}

void simulation_restart (SimulationState *state) {
  init_speed_rho (state);
  compute_f_equilibrium (state);
  init_f (state);
}

/* 
   updates simulation state: wave propagation, collisions
   with obstacles, etc.
*/
void simulation_update (SimulationState *state) {
  streaming (state);
  objects(state);
  compute_speed_rho (state);
  compute_f_equilibrium (state);
  collide (state);
}

/* momentum transmition (wave propagation)  */
void streaming (SimulationState *state) {
  int xL, xR; // X: left and right
  int yT, yB; // Y: top and bottom
  float *f_tmp, ***f;
  
  for (int x = 0; x < state->nx; ++x) {
    xL = x-1;
    xR = x+1;
    xL = (xL < 0)           ? state->nx-1 : xL;
    xR = (xR > state->nx-1) ? 0           : xR;
    for (int y = 0; y < state->ny; ++y) {
      yB = y-1;
      yT = y+1;
      yB   = (yB < 0)           ? state->ny-1 : yB;
      yT   = (yT > state->ny-1) ? 0           : yT;
      f_tmp = state->f_tmp[x][y];
      f = state->f;
      f_tmp[0]         = f[x][y][0];
      f_tmp[North]     = f[x][yB][North];
      f_tmp[South]     = f[x][yT][South];
      f_tmp[East]      = f[xL][y][East];
      f_tmp[West]      = f[xR][y][West];
      f_tmp[NorthEast] = f[xL][yB][NorthEast];
      f_tmp[NorthWest] = f[xR][yB][NorthWest];
      f_tmp[SouthEast] = f[xL][yT][SouthEast];
      f_tmp[SouthWest] = f[xR][yT][SouthWest];
    }
  }
  float ***buffer = state->f;
  state->f = state->f_tmp;
  state->f_tmp = buffer;
}

/* calculating some parameters of computation  */
void compute_speed_rho (SimulationState *state) {
  int nx = state->nx;
  int ny = state->ny;
  float r, vx, vy;
  float *f;

  for (int x = 0; x < nx; ++x) {
    for (int y = 0; y < ny; ++y) {
      r = 0.0;
      vx = vy = 0.0;
      f = state->f[x][y];
      for (int i = 0; i < Q; ++i) {
	vx += f[i] * c[i][0];
	vy += f[i] * c[i][1];
	r += f[i];
      }
      vx /= r;
      vy /= r;
      state->rho[x][y] = r;
      state->ux[x][y] = vx;
      state->uy[x][y] = vy;;
    }
  }
}

/* calculation f equilibrium for each direction  */
void compute_f_equilibrium (SimulationState *state) {
  for (int x = 0; x < state->nx; ++x) {
    for (int y = 0; y < state->ny; ++y) {
      float vx = state->ux[x][y];
      float vy = state->uy[x][y];
      float u_sqr = u_sqr = vx * vx + vy * vy;
      state->u_sqr[x][y] = u_sqr;
      u_sqr *= 0.5 * csSqd;
      float r = state->rho[x][y];
      for (int i = 0; i < Q; ++i) {
	float uci = vx * c[i][0] + vy * c[i][1];
	uci /= csSqd;
	state->f_eq[x][y][i] = r * w[i] *
	  (1.0 + uci * (1.0 + uci / 2.0) - u_sqr / 2.0);
      }
    }
  }
}

/* relaxation.. even waves need to relax...  */
void collide (SimulationState *state) {
  int nx = state->nx;
  int ny = state->ny;

  for (int x = 0; x < nx; ++x)
    for (int y = 0; y < ny; ++y)
      for (int i = 0; i < Q; ++i)	
	state->f[x][y][i] -= 
	  (state->f[x][y][i] - 
	   state->f_eq[x][y][i]) / tau;
}

void objects (SimulationState *state) {
  int x, y, i;
  float *buf;
  
  for (x = 0; x < state->nx; ++x)
    for (y = 0; y < state->ny; ++y)
      if (state->object[x][y] == Solid) {
	state->f_tmp[x][y][0] = state->f[x][y][0];
	state->f_tmp[x][y][East] = state->f[x][y][West];
	state->f_tmp[x][y][North] = state->f[x][y][South];
	state->f_tmp[x][y][West] = state->f[x][y][East];
	state->f_tmp[x][y][South] = state->f[x][y][North];
	state->f_tmp[x][y][NorthEast] = state->f[x][y][SouthWest];
	state->f_tmp[x][y][NorthWest] = state->f[x][y][SouthEast];
	state->f_tmp[x][y][SouthWest] = state->f[x][y][NorthEast];
	state->f_tmp[x][y][SouthEast] = state->f[x][y][NorthWest];
	buf = state->f_tmp[x][y];
	state->f_tmp[x][y] = state->f[x][y];
	state->f[x][y] = buf;
      } else if (state->object[x][y] == Absorber) {
	for (i = 0; i < Q; ++i)
	  state->f[x][y][i] = state->f_eq[x][y][i];
      }
}

/* -------------- INIT FUNCTIONS -------------------- */

/* allocating memory for simulation  */
void allocate_memory (SimulationState *state) {
  int x, y;
  
  state->f     = (float ***) malloc (sizeof(float **) * state->nx);
  state->f_eq  = (float ***) malloc (sizeof(float **) * state->nx);
  state->f_tmp = (float ***) malloc (sizeof(float **) * state->nx);
  state->ux    = (float **) malloc (sizeof(float *) * state->nx);
  state->uy    = (float **) malloc (sizeof(float *) * state->nx);
  state->u_sqr  = (float **) malloc (sizeof(float *) * state->nx);
  state->rho   = (float **) malloc (sizeof(float *) * state->nx);
  state->object = (int **) malloc (sizeof(int *) * state->nx);
  for (x = 0; x < state->nx; ++x) {
    state->f[x]      = (float **) malloc (sizeof(float *) * state->ny);
    state->f_eq[x]   = (float **) malloc (sizeof(float *) * state->ny);
    state->f_tmp[x]  = (float **) malloc (sizeof(float *) * state->ny);
    state->ux[x]     = (float *) malloc (sizeof(float) * state->ny);
    state->uy[x]     = (float *) malloc (sizeof(float) * state->ny);
    state->u_sqr[x]   = (float *) malloc (sizeof(float) * state->ny);
    state->rho[x]    = (float *) malloc (sizeof(float) * state->ny);
    state->object[x] = (int *) malloc (sizeof(int) * state->ny);
    for (y = 0; y < state->ny; ++y) {
      state->f[x][y]     = (float *) malloc (sizeof(float) * Q);
      state->f_eq[x][y]  = (float *) malloc (sizeof(float) * Q);
      state->f_tmp[x][y] = (float *) malloc (sizeof(float) * Q);
    }
  }
}

void init_speed_rho (SimulationState *state) {
  int x, y;
  
  for (x = 0; x < state->nx; ++x)
    for (y = 0; y < state->ny; ++y) {
      state->ux[x][y] = 0.0f;
      state->uy[x][y] = 0.0f;
      state->rho[x][y] = start_rho;
    }
}

void init_f (SimulationState *state) {
  int x, y, i;
  
  for (x = 0; x < state->nx; ++x)
    for (y = 0; y < state->ny; ++y)
      for (i = 0; i < Q; ++i)
	state->f[x][y][i] = state->f_eq[x][y][i];
}

void init_objects (SimulationState *state) {
  int x, y;

  for (x = 0; x < state->nx; ++x)
    for (y = 0; y < state->ny; ++y)
      if (x == 0 || x == state->nx-1 ||
	  y == 0 || y == state->ny-1)
	state->object[x][y] = Solid;
      else
	state->object[x][y] = Liquid;
}
