/***
    This file is part of "sound-waves" program.
    This program simulates sound waves behaviour in 2D space.
    Copyright (C) 2014  Sergey Dukanov <sergeydukanov@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***/

#include <math.h>
#include "lbm-physics.h"
#include "obstacles.h"

void find_isolated (int **object, int nx, int y);
void clear_isolated (SimulationState *s);

void solid_point (SimulationState *s, int x_pos, int y_pos) {
  int min_x, min_y, max_x, max_y;
  int x, y;
  int R = 3;
  
  // check range
  if (x_pos >= s->nx || x_pos < 0 || y_pos >= s->ny || y_pos < 0)
    return;
  // set boundaries
  min_x = (x_pos - R < 0 ? 0 : x_pos - R);
  max_x = (x_pos + R >= s->nx ? s->nx - 1 : x_pos + R);
  min_y = (y_pos - R < 0 ? 0 : y_pos - R);
  max_y = (y_pos + R >= s->ny ? s->ny - 1 : y_pos + R);
  for (x = min_x; x <= max_x; ++x)
    for (y = min_y; y <= max_y; ++y)
      s->object[x][y] = Solid;
}

void solid_line (SimulationState *s, int x1, int y1, int x2, int y2) {
  float vec_x;
  float vec_y;
  float distance;
  float i, x, y;

  // if not line
  if (x1 == x2 && y1 == y2) {
    solid_point (s, x1, y1);
    find_isolated (s->object, s->nx, s->ny);
    clear_isolated (s);
    return;
  }

  vec_x = x2 - x1;
  vec_y = y2 - y1;
  distance = sqrt(vec_x * vec_x + vec_y * vec_y);
  vec_x /= distance;
  vec_y /= distance;
  for(i = 0; i < distance; ++i) {
    x = x1 + vec_x * i;
    y = y1 + vec_y * i;
    solid_point (s, (int) x, (int) y);
  }
  find_isolated (s->object, s->nx, s->ny);
  clear_isolated (s);
}

void find_isolated (int **object, int nx, int ny) {
  int x, y;

  for(x = 1; x < (nx - 1); ++x)
    for(y = 1; y < (ny - 1); ++y) {
      if( object[x][y+1] != Liquid &&
	  object[x][y-1] != Liquid &&
	  object[x+1][y] != Liquid &&
	  object[x-1][y] != Liquid &&
	  object[x+1][y+1] != Liquid &&
	  object[x-1][y-1] != Liquid &&
	  object[x+1][y-1] != Liquid &&
	  object[x-1][y+1] != Liquid )
	object[x][y] = Isolated;
      else
	if(object[x][y] == Isolated)
	  object[x][y] = Solid;
    }
}

void clear_isolated (SimulationState *s) {
  int x, y, i;

  for (x = 1; x < (s->nx-1); ++x)
    for (y = 0; y < (s->ny-1); ++y) {
      if (s->object[x][y] == Isolated) {
	for (i = 0; i < Q; ++i)
	  s->f[x][y][i] = s->f_eq[x][y][i];
      }
    }
}
