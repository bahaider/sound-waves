/***
    This file is part of "sound-waves" program.
    This program simulates sound waves behaviour in 2D space.
    Copyright (C) 2014  Sergey Dukanov <sergeydukanov@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***/

#ifndef H_RENDER
#define H_RENDER

#include "lbm-physics.h"

typedef enum {
  Velocity, Rho
} draw_t;

typedef enum {
  BlackWhite, RainBow, Cyan, Mangenta
} color_t;

typedef enum {
  Relative, Absolute
} maxval_t;

typedef int pixel_size_t;

typedef struct {
  draw_t  drawing_type;
  color_t color_type;
  maxval_t maxval_type;
  pixel_size_t pixel;

  float *value;
  unsigned char *bitmap;
  
  int nx;
  int ny;
} VisualizationState;

VisualizationState *visualization_create (int nx, int ny);
void render (SimulationState *sim, VisualizationState *vis);
unsigned char *get_bitmap (VisualizationState *vis);

#endif
