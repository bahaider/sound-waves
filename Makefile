CC = gcc
CFLAGS = -c -std=c99 -Wall
LDFLAGS = -lm -lGL -lglut
SOURCES = main.c lbm-physics.c render.c obstacles.c
OBJECTS = $(SOURCES:.c=.o)
EXECUTABLE = a.out

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf *.o $(EXECUTABLE)
