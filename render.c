/***
    This file is part of "sound-waves" program.
    This program simulates sound waves behaviour in 2D space.
    Copyright (C) 2014  Sergey Dukanov <sergeydukanov@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***/

#include <stdlib.h>
#include "render.h"
#include "lbm-physics.h"

void calculate_values_velocity (VisualizationState *vis, float **u_sqr);
void calculate_values_rho      (VisualizationState *vis, float **rho);
void render_liquids (VisualizationState *vis);
void render_objects (VisualizationState *vis, int **object);
void allocate_render_memory (VisualizationState *vis);

VisualizationState *visualization_create (int nx, int ny) {
  VisualizationState *vis = malloc (sizeof(VisualizationState));
  vis->color_type = BlackWhite;
  vis->drawing_type = Rho;
  vis->maxval_type = Relative;
  vis->pixel = 1; // disabled
  vis->nx = nx;
  vis->ny = ny;
  allocate_render_memory (vis);
  return vis;
}

void render (SimulationState *sim, VisualizationState *vis) {
  if (vis->drawing_type == Velocity)
    calculate_values_velocity (vis, sim->u_sqr);
  else
    calculate_values_rho (vis, sim->rho);
  render_liquids (vis);
  render_objects (vis, sim->object);
}

void calculate_values_velocity (VisualizationState *vis, float **u_sqr) {
  static float u_sqr_max_absolute = 0.0001;
  float u_sqr_max = 0.0;
  int nx = vis->nx, ny = vis->ny;
  int x, y;
  
  // find max value
  for (x = 0; x < nx; ++x)
    for (y = 0; y < ny; ++y)
      if (u_sqr[x][y] > u_sqr_max)
	u_sqr_max = u_sqr[x][y];
  
  // choose value to use: absolute or relative
  // ### still does not work properly :( ###
  if (vis->maxval_type == Relative) {
  } else if (vis->maxval_type == Absolute) {
    if (u_sqr_max > u_sqr_max_absolute)
      u_sqr_max_absolute = u_sqr_max;
    else
      u_sqr_max = u_sqr_max_absolute;
  }
  
  // calculate values
  for (x = 0; x < nx; ++x)
    for (y = 0; y < ny; ++y)
      vis->value[x*ny + y] = u_sqr[x][y] / u_sqr_max;
}

void calculate_values_rho (VisualizationState *vis, float **rho) {
  float max_rho = 0.0;
  float min_rho = 1.0;
  static float max_rho_absolute = 0.0;
  static float min_rho_absolute = 1.0;
  int nx = vis->nx, ny = vis->ny;
  int x, y;

  // find max and min rho valueses
  for (x = 0; x < nx; ++x) {
    for (y = 0; y < ny; ++y) {
      max_rho = (rho[x][y] > max_rho ? rho[x][y] : max_rho);
      min_rho = (rho[x][y] < min_rho ? rho[x][y] : min_rho);
    }
  }

  // deside, what value to use: absolute or relative
  if (vis->maxval_type == Relative) {
  } else if (vis->maxval_type == Absolute) {
    if (min_rho < min_rho_absolute)
      min_rho_absolute = min_rho;
    if (max_rho > max_rho_absolute)
      max_rho_absolute = max_rho;
    min_rho = min_rho_absolute;
    max_rho = max_rho_absolute;
  }
  
  // calculate values
  max_rho = max_rho - min_rho;
  for (x = 0; x < nx; ++x)
    for (y =0; y < ny; ++y)
      vis->value[x*ny+y] = (rho[x][y] - min_rho) / max_rho;
}

void render_liquids (VisualizationState *vis) {
  float *v = vis->value;
  unsigned char *pixels = vis->bitmap;
  unsigned int color;
  int nx = vis->nx, ny = vis->ny;
  int x, y, i;
  
  // draw field
  switch (vis->color_type) {
  case BlackWhite:
    for (x = 0; x < nx; ++x) {
      for (y = 0; y < ny; ++y) {
	color = (unsigned) (v[y*nx + x] * 255.0);
	i = ((nx - x - 1) * ny + y) * 4;
       	pixels[i + 0] =	color;
	pixels[i + 1] = color;
	pixels[i + 2] = color;
	pixels[i + 3] = 255;
      }
    }
    break;
  case RainBow:
    // Arkadiy Prigojin (c) - 2013
    for (x = 0; x < nx; ++x) {
      for (y = 0; y < ny; ++y) {
	color = (unsigned) (255.0 * 4.0 * v[y*nx + x]);
	i = ((nx - x - 1) * ny + y) * 4;
	if (color <= 1 * 255) {
	  pixels[i + 0] = 0;
	  pixels[i + 1] = color;
	  pixels[i + 2] = 255;
	  pixels[i + 3] = 255;
	} else if (color <= 2 * 255) {
	  pixels[i + 0] = 0;
	  pixels[i + 1] = 255;
	  pixels[i + 2] = 2 * 255 - color;	
	  pixels[i + 3] = 255;
	} else if (color <= 3 * 255) {
	  pixels[i + 0] = color - 2 * 255;
	  pixels[i + 1] = 255;
	  pixels[i + 2] = 0;
	  pixels[i + 3] = 255;
	} else {
	  pixels[i + 0] = 255;
	  pixels[i + 1] = 4 * 255 - color;
	  pixels[i + 2] = 0;
	  pixels[i + 3] = 255;
	}
      }
    }
    // <- that's beautiful!
    break;
  case Cyan:
    for (x = 0; x < nx; ++x) {
      for (y = 0; y < ny; ++y) {
	color = (unsigned)(v[y*nx + x] * 255.0);
	i = ((nx - x - 1) * ny + y) * 4;
	pixels[i + 0] = 0;
	pixels[i + 1] = color;
	pixels[i + 2] = color;
	pixels[i + 3] = 255;
      }
    }
    break;
  case Mangenta:
    for (x = 0; x < nx; ++x) {
      for (y = 0; y < ny; ++y) {
	color = (unsigned )(v[y*nx + x] * 255.0);
	i = ((nx - x - 1) * ny + y) * 4;
	pixels[i + 0] = color;
	pixels[i + 1] = 0;
	pixels[i + 2] = color;
	pixels[i + 3] = 255;
      }
    }
    break;
  }
}

void render_objects (VisualizationState *vis, int **object) {
  static const unsigned char RGBA_color_solid[4]    = {127, 127,  76, 255};
  static const unsigned char RGBA_color_absorber[4] = {100, 100,  25, 255};
  static const unsigned char RGBA_color_isolated[4] = {200, 200, 135, 255};
  int nx = vis->nx, ny = vis->ny;
  unsigned char *pixels = vis->bitmap;
  int x, y, i, c;

  for (x = 0; x < nx; ++x) {
    for (y = 0; y < ny; ++y) {
      if (object[x][y] == Solid)  {
	i = ((ny - y - 1) * nx + x) * 4;
	for (c = 0; c < 4; ++c)
	  pixels[i + c] = RGBA_color_solid[c];
      } else if (object[x][y] == Absorber) {
	i = ((ny - y - 1) * nx + x) * 4;
	for (c = 0; c < 4; ++c)
	  pixels[i + c] = RGBA_color_absorber[c];
      } else if (object[x][y] == Isolated) {
	i = ((ny - y - 1) * nx + x) * 4;
	for (c = 0; c < 4; ++c)
	  pixels[i + c] = RGBA_color_isolated[c];
      }
    }
  }
}

/* allocate memory for graphics needs */
void allocate_render_memory (VisualizationState *vis) {
  vis->bitmap = (unsigned char *) malloc (sizeof (unsigned char *) * 
					  vis->nx * vis->ny * 4);
  vis->value = (float *) malloc (sizeof (float) * vis->nx * vis->ny);
}

unsigned char *get_bitmap (VisualizationState *vis) {
  return vis->bitmap;
}
