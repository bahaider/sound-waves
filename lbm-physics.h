/***
    This file is part of "sound-waves" program.
    This program simulates sound waves behaviour in 2D space.
    Copyright (C) 2014  Sergey Dukanov <sergeydukanov@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***/

#ifndef H_LBM_PHYSICS
#define H_LBM_PHYSICS

/* simulation parameters */
#define Q 9
#define csSqd 1.0f / 3.0f
#define cs 1.0f / sqrt(3.0f)
#define start_rho 1.0f

enum direction {
  East = 1, North, West, South,
  NorthEast, NorthWest, SouthWest, SouthEast
};

enum object_type {
  Solid, Liquid, Absorber, Isolated
};

typedef struct {
  int nx;
  int ny;
  float ***f;
  float ***f_eq;
  float **ux;
  float **uy;
  float **u_sqr;
  float **rho;
  float ***f_tmp;
  int   **object;
} SimulationState;

SimulationState *simulation_create (int nx, int ny);
void simulation_init (SimulationState *state);
void simulation_update (SimulationState *state);
void simulation_restart (SimulationState *state);

#endif

