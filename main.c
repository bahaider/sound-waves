/***
    This file is part of "sound-waves" program.
    This program simulates sound waves behaviour in 2D space.
    Copyright (C) 2014  Sergey Dukanov <sergeydukanov@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
***/

#include <stdio.h>
#include <GL/glut.h>
#include "lbm-physics.h"
#include "obstacles.h"
#include "render.h"

SimulationState *simulation;
VisualizationState *visualization;

void initialization (int *nx, int *ny, int argc, char *argv[]);
void display (void);
void mouse (int button, int state, int x, int y);
void keyboard (unsigned char key, int x, int y);
void tick (int);

void flush_to_display (VisualizationState *vis);

int main (int argc, char *argv[]) {
  /* a journey of a thousand miles begins with a single step */
  int nx, ny;
  
  initialization (&nx, &ny, argc, argv);
  simulation = simulation_create (nx, ny);
  simulation_init (simulation);
  visualization = visualization_create (nx, ny);

  /* this is an inittialization of graphics */
  glutInit (&argc, argv);
  glutInitDisplayMode (GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowPosition (200, 200);
  glutInitWindowSize (nx, ny);
  glutCreateWindow ("sound-waves");
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  glOrtho (-1, nx, ny, -1, -1, 1);
  glClearColor (0, 0, 0, 0);
  glutTimerFunc (40, tick, 0);
  glutDisplayFunc (display);
  glutMouseFunc (mouse);
  glutKeyboardFunc (keyboard);
  glutMainLoop ();
  return 0;
}

void initialization (int *nx, int *ny, int argc, char *argv[]) {
  switch (argc) {
  case 3:
    *nx = atoi (argv[1]);
    *ny = atoi (argv[2]);
    break;
  case 2:
    *nx = *ny = atoi (argv[1]);
    break;
  default:
    *nx = *ny = 256;
  }
}

void display (void) {
  glClear(GL_COLOR_BUFFER_BIT);
  render (simulation, visualization);
  flush_to_display (visualization);
  glFlush();  
  glutSwapBuffers();
}

void mouse (int button, int button_state, int x_pos, int y_pos) {
  int i;
  static int old_x_pos, old_y_pos;
  
  if (button_state == GLUT_DOWN) {
    old_x_pos = x_pos;
    old_y_pos = y_pos;
  } else if (button_state == GLUT_UP) {
    switch  (button) {
    case GLUT_LEFT_BUTTON:
      for (i = 0; i < Q; ++i) {
	simulation->f[x_pos][y_pos][i] += 0.03;
      }
      break;
    case GLUT_MIDDLE_BUTTON:
      break;
    case GLUT_RIGHT_BUTTON:
      solid_line (simulation, old_x_pos, old_y_pos, x_pos, y_pos);
      break;
    }
  }
}

void keyboard (unsigned char key, int x, int y) {
  /* changes visualization types */
  switch (key) {
  case '1':
    visualization->drawing_type = Velocity;
    break;
  case '2':
    visualization->drawing_type = Rho;
    break;
  case 'r':
    visualization->maxval_type = Relative;
    break;
  case 'a':
    visualization->maxval_type = Absolute;
    break;
  case 'c':
    visualization->color_type++;
    visualization->color_type %= 4;
    break;
  case 'w':
    simulation_restart (simulation);
    break;
  default:
    fprintf (stderr, "error: undefined key\n");
  }
}

void tick (int t) {
  simulation_update (simulation);
  glutPostRedisplay();
  glutTimerFunc(40, tick, 0);
}

void flush_to_display (VisualizationState *vis) {
  glDrawPixels(vis->nx, vis->ny, GL_RGBA, GL_UNSIGNED_BYTE, vis->bitmap);
}
